var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListKingCounty.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;
    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow', curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp("./dist/EntryPage", function(err) {
        if (err) return cb(err);
        console.log("create folder path", "./dist/EntryPage");
    });

    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 30000) {
        finaltimer = 10000;
    }
    //add timeout - reduce suspicious of crawling
    setTimeout(function() {




        console.log('Check exist ');
        console.log('start download ', urlLink);


        var commandRun = 'sleep 1.12s && curl "' + (urlLink) + '" ';
        var commandRun = 'sleep 1.12s && phantomjs PageDownload6.js "' + (urlLink) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(curRow);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);



    }, finaltimer);

});
var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        //take out all html   
        return ($("<p>" + _val + "</p>").text());
    }

    curLink = urllink;
    var curRow = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };

    var companyInfo = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };
    if ($('.map-container h3')) {
        companyInfo.name = sanitizeDash($('.map-container h3').html());
    }

    if ($('.map-container address')) {
        companyInfo.address = sanitizeDash($('.map-container address').html());
        companyInfo.address = companyInfo.address.substring(0, companyInfo.address.indexOf("Monday thru"));
        companyInfo.address = companyInfo.address.replace("\r", "");

    }

    if ($(":contains('Pet Information Line')")) {
        companyInfo.contact = sanitizeDash($(":contains('Pet Information Line')").html());
    }
    if ($(".media-body").eq(3)) {
        companyInfo.phone = sanitizeDash($(".media-body").eq(3).html());
    }
    if ($(".media-body").eq(2)) {
        companyInfo.email = sanitizeDash($(".media-body").eq(2).html());
    }


    companyInfo.lat = 47.4080889;
    companyInfo.lng = -122.2574875;

    companyInfo.startHourWeekday = "12:00 am"
    companyInfo.endHourWeekday = "06:00 pm"
    companyInfo.startHourWeekend = "12:00 pm"
    companyInfo.endHourWeekend = "05:00 pm"



    var filename = "./dist/ListingKingCounty.txt";
    fs.writeFileSync(filename, JSON.stringify(companyInfo));
    console.log("written to file ", filename);



    var strlist = "";


    $('.modal').each(function(idx, elemelem) {

        var curLink = urllink;
        var initialhtml = $(this).find('.modal-body').html();


        var curhtml = initialhtml.replace('<!--', '');
        curhtml = curhtml.replace('-->', '');
        curhtml = "<div>" + curhtml + "</div>";

        console.log("curhtml");
        $curelem = $(curhtml);

        if ($(this).find('.modal-title strong')) {
            curRow.id = $(this).find('.modal-title strong').html();

        }
        curRow.name = curRow.id;
        if ($curelem.find('p:nth-child(1)')) {
            curRow.receivedon = $curelem.find('p:nth-child(1)').html()
            curRow.receivedon = curRow.receivedon.replace('Received on:', '');
        }
        if ($curelem.find('p:nth-child(2)')) {
            curRow.prevlocation = $curelem.find('p:nth-child(2)').html()
            curRow.prevlocation = curRow.prevlocation.replace('Found Near:', '');
        }
        if ($curelem.find('p:nth-child(3)')) {
            curRow.species = sanitizeDash($curelem.find('p:nth-child(3)').html());
            curRow.species = curRow.species.replace('Description:', '');
        }
        if ($curelem.find('p:nth-child(4)')) {
            curRow.age = $curelem.find('p:nth-child(4)').html()
            curRow.species = curRow.species.replace('Age:', '');
        }
        if ($curelem.find('p:nth-child(5)')) {
            curRow.curlocation = sanitizeDash($curelem.find('p:nth-child(5)').html());
            curRow.curlocation = curRow.curlocation.replace('Current Location:', '');
        }
        if ($(this).find('.modal-body .img-responsive')) {
            curRow.photo = $(this).find('.modal-body .img-responsive').prop('src');

            var filename = "./dist/kingcountyimage" + curRow.id + ".jpg"
            filename= "dist/kingcountyimage" + curRow.id .replace(/[^a-z0-9]/gi, '_').toLowerCase()+ ".jpg"
            curRow.filename = filename;


            var url = (curRow.photo);
            
 

            download(url,  "./dist").then((data) => {
                fs.writeFileSync(filename, data);
                console.log('done download data' ,data);
                console.log('done download - ' + url + ' into ' + filename);
            });
 

            var additionalHeader=  '-H "Pragma: no-cache" -H "Accept-Encoding: gzip, deflate, sdch" -H "Accept-Language: en-US,en;q=0.8" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Cache-Control: no-cache" -H "Connection: keep-alive" --compressed ';
            var commandRun = 'sleep .1s && curl "' + url + '" '+ additionalHeader+ ' > "' + filename+ '"'     ;
            // var commandRun = 'sleep  .1s && phantomjs PageDownload6.js "' + (url) + '" ';
            console.log('commandRun',commandRun);
            try {

                exec(commandRun, function(status, output) {
                    console.log('Exit status:', status);
                    console.log('Program output:');
                    console.log('done download  url', url);
                    console.log('done download  filename', filename);

                    console.log('done download  output', output);
                    if (!output) {
                        return;
                    }
                    console.log('output',output);

                     fs.writeFileSync(filename, output);

                });

            } catch (e) {
                console.log(e);

            }



        }


        var list = initialhtml.split("<p></p>");

        if (list && list[2]) {
            curRow.description = list[2];
        }




        console.log(" curRow ", curRow);
        if (curRow.id && curRow.name) {
            listData.push(curRow);
            strlist = strlist + "," + JSON.stringify(curRow);
        }



    });

    var filename = "./dist/ResultKingCounty.txt";
    var liststr = "[{}" + strlist + "]";
    fs.writeFileSync(filename, liststr);
    console.log("written to file", filename);


};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
