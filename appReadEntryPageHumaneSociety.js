var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListHumaneSociety.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

    var strjson = "[{}";
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var finalList = [];
var downloadedEntryPathList = [];
//download all the first page listing
var ref =""
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
      ref = urlInfo[2];
    urllink = urlLink;



    var curRow = {
        "ref": ref,
        "link": urllink
    }
    var generalData = fs.readFileSync('./dist/ListingIndexHumaneSociety'+ref+'.txt', 'utf8');
    generalData = JSON.parse(generalData);
    totalPage = parseInt(generalData.totalPage);
    pagelocalfolder = generalData.pagelocalfolder;

    for (var i = 1; i <= totalPage; i++) {
        try {
            // read all files 
            var pageContentData = fs.readFileSync(pagelocalfolder + "/" + i + '.txt', 'utf8');

            var $ = cheerio.load(pageContentData);
            var sanitizeDash = function(_val) {
                //take out all html   
                return ($("<p>" + _val + "</p>").text());
            }

            // grab all detail links
            $('.ResultsTable').find('tr').each(function(iddx, elemelem) {

                curRow.species=  ref.replace('HumaneSociety','');  
                curRow.species=  curRow.species.replace('Dogs','Dog');  
                curRow.species=  curRow.species.replace('Cats','cat');  
                if ($(this).find('.TableContent2').length > 1 || $(this).find('.TableContent1').length > 1) {
                    $curElem = $(this);

                    if ($curElem.find('img').prop('src')) {
                        curRow.photo = "http://petharbor.com/" + $curElem.find('img').prop('src')
                    } 
                    if ($curElem.find('td').eq(1).find('font').eq(0)) {
                        curRow.name = sanitizeDash($curElem.find('td').eq(1).find('font').eq(0))
                    }

                    if ($curElem.find('td').eq(1).find('font').eq(1)) {
                        curRow.id = sanitizeDash($curElem.find('td').eq(1).find('font').eq(1))
                    }


                    if ($curElem.find('td').eq(1)) {
                        var html = $curElem.find('td').eq(1).html();
                        htmllist = html.split("<br>");

                        curRow.breed = sanitizeDash(htmllist[htmllist.length-3]);
                        curRow.sex = sanitizeDash(htmllist[htmllist.length-2].split("|")[0]);
                        curRow.age = sanitizeDash(htmllist[htmllist.length-2].split("|")[1]);
                        curRow.desc = sanitizeDash(htmllist[htmllist.length- 1]);
                    }

                    if ($curElem.find('td').eq(0).find('a')) {
                        var detailurl =$curElem.find('td').eq(0).find('a').prop('href')
                        var detailurl = "http://petharbor.com/" + detailurl
                        var petId = getParameterByName('ID', detailurl);
                    
                        var localfolder = commonFolder + "/" + ref + "/"
                        curRow.detailurl =   detailurl;
                        curRow.detailurlfile = localfolder + petId + ".txt"


                        mkdirp(localfolder, function(err) {
                            if (err) return cb(err);
                            console.log("create folder path", localfolder);
                        });

                        //download details 
                        if (fs.existsSync(curRow.detailurlfile)) {
                            console.log('skipped download pageurl', curRow.detailurl);
                            console.log('skipped download pagelocalpath', curRow.detailurlfile);
                        } else {

                            //listing and details page
                            var commandRun = 'sleep 2.13s &&curl "' + (curRow.detailurl) + '" > ' + curRow.detailurlfile + '';
                            try {

                                exec(commandRun, function(status, output) {
                                    console.log('Exit status:', status);
                                    console.log('done download pageurl', curRow.detailurl);
                                    console.log('done download pagelocalpath', curRow.detailurlfile);
                                });

                            } catch (e) {
                                console.log(e);

                            }
                        }
                    }





                }

                console.log('row', curRow);
                finalList.push(curRow);

                strjson+="," + JSON.stringify(curRow)
  

            });

        } catch (e) {

        }

    }


    var filename = "./dist/ResultHumaneSociety"+ref+".txt";
    // fs.writeFileSync(filename, JSON.stringify(finalList));
    console.log("write to file ", filename); 
    fs.writeFileSync(filename,strjson +"]" );


});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
