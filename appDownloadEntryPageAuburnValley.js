var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListAuburnValley.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;
    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow', curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp(commonFolder, function(err) {
        if (err) return cb(err);
        console.log("create folder path", commonFolder);
    });

    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 30000) {
        finaltimer = 10120;
    }







    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        console.log('start download ', urlLink);



        //listing and details page
        var commandRun = 'phantomjs  --web-security=no PageDownload0.js "' + (urlLink) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(curRow);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);




    }, finaltimer);





});
var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        //take out all html   
        return ($("<p>" + _val + "</p>").text());
    }

    curLink = urllink;
    var companyInfo = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };
    var curRow = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };



    if (($(".textwidget").eq(0)).parent()) {
        var curelem = $(".textwidget").eq(0).parent()
        var curhtml = curelem.html()
        console.log();
        var curlist = curhtml.split("<br>");

        companyInfo.name = sanitizeDash(curlist[0]);
        companyInfo.name = "Auburn Valley Humane Society";
        companyInfo.address = curlist[1];
        companyInfo.phone = curlist[2].replace("Phone:", "");;
        companyInfo.fax = curlist[3];
    }
    companyInfo.lat = 47.2643133
    companyInfo.lng = -122.2314338


    companyInfo.startHourWeekday = "10:00 am"
    companyInfo.endHourWeekday = "06:00 pm"

    companyInfo.startHourWeekend = "12:00 am"
    companyInfo.endHourWeekend = "04:00 pm"

 

    var filename = "./dist/ListingAuburnValley.txt";
    fs.writeFileSync(filename, JSON.stringify(companyInfo));
    console.log("written to file ", filename);


    var strlist = "";
    $('.list-item').each(function(idx, elemelem) {

        var curLink = $(this).find('.list-animal-name a').prop('href');
        curLink = urllink;

        if ($(this).find('.list-animal-name a')) {

            curRow.name = sanitizeDash($(this).find('.list-animal-name a').html());
            if (curRow.name == 'null') {
                curRow.name = null;
            }
        }


        if ($(this).find('.list-animal-id')) {
            curRow.id = sanitizeDash($(this).find('.list-animal-id').html());
        }
        if ($(this).find('.list-animal-name a').prop('href')) {

            detailurl = "http://ws.petango.com/webservices/adoptablesearch/" + $(this).find('.list-animal-name a').prop('href');
            detailurl = detailurl.replace(/&amp;/gi, '');
            curRow.detailurl = detailurl;

            var detailFolder = commonFolder + "/" + _elem.ref;
            mkdirp(detailFolder, function(err) {
                if (err) return cb(err);
                console.log("create folder path", detailFolder);
            });


            curRow.detailurlfile = detailFolder + "/" + curRow.id + ".txt";

            if (!fs.existsSync(curRow.detailurlfile)) {

                var commandRun = '  curl "' + (curRow.detailurl) + '" > ' + curRow.detailurlfile + '  ';
                try {
                    console.log('commandRun', commandRun);
                    exec(commandRun, function(status, output) {
                        console.log('Exit status:', status);

                        console.log('done download urlLink', detailurl);
                        console.log('done download urlPath', curRow.detailurlfile);


                        if (!detailurl || !output) {
                            return;
                        }

                        // fs.writeFileSync(curRow.detailurlfile, output);

                    });

                } catch (e) {
                    console.log(e);

                }

            } else {

                console.log('skipped download urlLink', detailurl);
                console.log('skipped download urlPath', curRow.detailurlfile);
            }
        }
        if ($(this).find('.list-animal-photo')) {
            curRow.photo = ($(this).find('.list-animal-photo').prop('src'));
            if (curRow.photo && curRow.photo.indexOf("http") < 0) {
                curRow.photo = "http:" + curRow.photo;
            }
        }
        if ($(this).find('.list-anima-species')) {
            curRow.species = sanitizeDash($(this).find('.list-anima-species').html());
        }
        if ($(this).find('.list-animal-sexSN')) {
            curRow.sex = sanitizeDash($(this).find('.list-animal-sexSN').html());
        }
        if ($(this).find('.list-animal-breed')) {
            curRow.breed = sanitizeDash($(this).find('.list-animal-breed').html());
        }
        if ($(this).find('.list-animal-age')) {
            curRow.age = sanitizeDash($(this).find('.list-animal-age').html());
        }

        if ($(this).find('.list-animal-foundtype')) {
            curRow.foundtype = sanitizeDash($(this).find('.list-animal-foundtype').html());
        }

        // console.log(" curRow ", curRow);
        if (curRow.id && curRow.name) {
            listData.push(curRow);
            strlist = strlist + "," + JSON.stringify(curRow);
        }

    });

    var filename = "./dist/ResultAuburnValley.txt";
    var liststr = "[{}" + strlist + "]";
    fs.writeFileSync(filename, liststr);
    console.log("written to file ", filename);


};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
