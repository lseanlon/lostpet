var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListKitsap.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var finalList = [];
var downloadedEntryPathList = [];
//download all the first page listing

urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;



    var curRow = {
        "ref": ref,
        "link": urllink
    }
    var generalData = fs.readFileSync('./dist/ListingIndexKitsap.txt', 'utf8');
    generalData = JSON.parse(generalData);
    totalPage = parseInt(generalData.totalPage);
    pagelocalfolder = generalData.pagelocalfolder;


    var strjson = "[{}";
    for (var i = 1; i <= totalPage; i++) {
        try {
            // read all files

            var pageContentData = fs.readFileSync(pagelocalfolder + "/" + i + '.txt', 'utf8');

            console.log('seeking pageContentData', pagelocalfolder + "/" + i + '.txt');
            var $ = cheerio.load(pageContentData);
            var sanitizeDash = function(_val) {
                //take out all html   
                return ($("<p>" + _val + "</p>").text());
            }


            // grab all detail links
            $('table').eq(1).find('tr').each(function(iddx, elemelem) {

                if ($(this).find('td').length > 1) {
                    $curElem = $(this);
 
                    if ($curElem.find('img').prop('src')) {
                        curRow.photo = "http://kitsaplostpets.org/" + $curElem.find('img').prop('src')
                    }

                    if ($curElem.find('td').eq(1).find('span')) {
                        var listData = $curElem.find('td').eq(1).find('span').html();
                        listData = listData.split("<br>");
                        // <span class="text">found : <br>Gender: female, Color: Solid Gray<br>Where: Red Oaks St, Town: Silverdale (98383)<br>Date found: 2017-01-08</span>
                        var firstrowdata = sanitizeDash(listData[1]).split(",")
                        curRow.sex = sanitizeDash(firstrowdata[0]);
                        curRow.sex = curRow.sex.replace('Gender:', '');

                        curRow.color = sanitizeDash(firstrowdata[1]);
                        curRow.color = curRow.color.replace('Color:', '');

                        curRow.datefound = sanitizeDash(listData[3]);
                        curRow.datefound = curRow.datefound.replace('Date found:', '');

                        curRow.location = sanitizeDash(listData[2]);
                        curRow.location = curRow.location.replace('Where:', '');

                    }

                    if ($curElem.find('td').eq(2).find('a')) {
                        var detailurl = $curElem.find('td').eq(2).find('a').prop('href')
                        var petId = getParameterByName('petID', detailurl);
                        curRow.id = petId;
                        var localfolder = commonFolder + "/" + ref + "/"
                        curRow.detailurl = "http://kitsaplostpets.org/" + detailurl;
                        curRow.detailurlfile = localfolder + petId + ".txt"


                        mkdirp(localfolder, function(err) {
                            if (err) return cb(err);
                            console.log("create folder path", localfolder);
                        });

                        //download details 
                        //
                        console.log('seeking pageurl', curRow.detailurl);
                        if (fs.existsSync(curRow.detailurlfile)) {
                            console.log('skipped download pageurl', curRow.detailurl);
                            console.log('skipped download pagelocalpath', curRow.detailurlfile);
                        } else {

                            //listing and details page
                            var commandRun = 'sleep 1.633s &&curl "' + (curRow.detailurl) + '" > ' + curRow.detailurlfile + '';
                            try {

                                exec(commandRun, function(status, output) {
                                    console.log('Exit status:', status);
                                    console.log('done download pageurl', curRow.detailurl);
                                    console.log('done download pagelocalpath', curRow.detailurlfile);
                                });

                            } catch (e) {
                                console.log(e);

                            }
                        }
                    }




                }

                console.log('row', curRow);
                strjson+="," + JSON.stringify(curRow)
 
                finalList.push(curRow); 

            });

        } catch (e) {

        }


    }


    var filename = "./dist/ResultKitsap.txt";
    // fs.writeFileSync(filename, JSON.stringify(finalList));
    fs.writeFileSync(filename,strjson +"]" );



});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
