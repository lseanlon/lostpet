<?php
/**
 * Writes new posts into wordpress programatically
 *
 * @package WordPress
 */

/** Make sure that the WordPress bootstrap has run before continuing. */
require(dirname(__FILE__) . '/wp-load.php'); 
require_once(ABSPATH . 'wp-config.php'); 
require_once(ABSPATH . 'wp-includes/wp-db.php'); 
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php'); 

global $user_ID; 
function isJsonKeyExist($row,$keyName){
  return  !empty( $row[ $keyName ])   ;
} 

function performInsertion($filename,$locationfilename){


  try{
      echo "BEGIN "  ; 

      //read from file 
      $folderPath= "./dist/";
      $json_data = file_get_contents($folderPath . $filename );  
      $jsonList = json_decode($json_data, true);

 
      $json_placedata = file_get_contents($folderPath . $locationfilename );  
      $jsonPlace = json_decode($json_placedata, true);


      $totalRow= count($jsonList); 

      echo "<br>The  totalRow is: $totalRow  <br>"  ;  
   

        for ($y =  0; $y <  ($totalRow); $y++) {
          $curRow = $jsonList[$y]; 

          $ref =isJsonKeyExist($curRow,"ref")? $curRow["ref"] :"-";  
          $id =isJsonKeyExist($curRow,"id")? $curRow["id"] :"-"; 
          $name =isJsonKeyExist($curRow,"name")? $curRow["name"] :"-";  
          $title =isJsonKeyExist($curRow,"name")? $curRow["name"] :"-"; 
          $title =isJsonKeyExist($curRow,"name")? $curRow["name"] : $id; 
          echo "<br>The  title is:  <br>"  . $title; 

          $link =isJsonKeyExist($curRow,"link")? $curRow["link"] :"-"; 
          $photo =isJsonKeyExist($curRow,"photo")? $curRow["photo"] :"-";
          if( strpos( $photo,"http")>=0){ 
          }
          else{ 
            $photo="http:" . $photo; 
          }
          $species =isJsonKeyExist($curRow,"species")? $curRow["species"] :"";  

          $sex =isJsonKeyExist($curRow,"sex")? $curRow["sex"] :"-"; 
          $breed =isJsonKeyExist($curRow,"breed")? $curRow["breed"] :"-"; 
          $foundtype =isJsonKeyExist($curRow,"foundtype")? $curRow["foundtype"] :"-";
          
          $age =isJsonKeyExist($curRow,"age")? $curRow["age"] :"-";
          $datefound =isJsonKeyExist($curRow,"datefound")? $curRow["datefound"] :"-";
          $location =isJsonKeyExist($curRow,"location")? $curRow["location"] :"-";
          $size =isJsonKeyExist($curRow,"size")? $curRow["size"] :"-";
          $color =isJsonKeyExist($curRow,"color")? $curRow["color"] :"-";
          $pattern =isJsonKeyExist($curRow,"pattern")? $curRow["pattern"] :"-";

          $weight =isJsonKeyExist($curRow,"weight")? $curRow["weight"] :"-";
          $tail =isJsonKeyExist($curRow,"tail")? $curRow["tail"] :"-";
          $eyes =isJsonKeyExist($curRow,"eyes")? $curRow["eyes"] :"-";
          $coat =isJsonKeyExist($curRow,"coat")? $curRow["coat"] :"-";
          $ears =isJsonKeyExist($curRow,"ears")? $curRow["ears"] :"-";
          $declawed =isJsonKeyExist($curRow,"declawed")? $curRow["declawed"] :"-";
          $reporttype =isJsonKeyExist($curRow,"reporttype")? $curRow["reporttype"] :"-";


          $description =isJsonKeyExist($curRow,"description")? $curRow["description"] :"-";
          $receivedon =isJsonKeyExist($curRow,"receivedon")? $curRow["receivedon"] :"-";

          $curlocation =isJsonKeyExist($curRow,"curlocation")? $curRow["curlocation"] :"-";
          $prevlocation =isJsonKeyExist($curRow,"prevlocation")? $curRow["prevlocation"] :"-";
          $deptPhoneNumbers =isJsonKeyExist($curRow,"deptPhoneNumbers")? $curRow["deptPhoneNumbers"] :"";
         $contact =isJsonKeyExist($curRow,"contact")? $curRow["contact"] :"";

         $shelther =isJsonKeyExist($jsonPlace,"name")? $jsonPlace["name"] :"-";  


         $imagefilename = null;
         $imagefilename =isJsonKeyExist($curRow,"filename")? $curRow["filename"] :"";  
          if(!empty( $imagefilename)){
              $photo = "http://bringpawshome.com/" . $imagefilename;
          }


             echo "<br>The breed is:  <br>"  . $breed;  
             echo "<br>Creating / Getting category for:   $breed <br>" ; 
             $categoryId = wp_create_category($breed); 
             echo "<br>categoryId: $categoryId "  ;  

            // $desc="[mashshare]";
            $desc=" "; 

            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Source Link:</b> $link";
            $desc =$desc. "</p>";
            $desc= $desc."<p>";

            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Reference:</b> $ref";
            $desc =$desc. "</p>";
    
           $desc= $desc."<p>";
            $desc =$desc. "<b>Shelther:</b> $shelther";
            $desc =$desc. "</p>";


            $desc= $desc."<p>";
            $desc =$desc. "<b>Id: $id </b>";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Name:</b> $name";
            $desc =$desc. "</p>";

            $desc =$desc. "<b>Species:</b> $species";
            $desc =$desc. "</p>";

            $desc= $desc."<p>"; 
            $desc =$desc. "<b>Sex:</b> $sex";
            $desc =$desc. "</p>";
            $desc= $desc."<p>";

            $desc =$desc. "<b>Breed:</b> $breed";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Found type:</b> $foundtype";
            $desc =$desc. "</p>";


            $desc= $desc."<p>";
            $desc =$desc. "<b>Age:</b> $age";
            $desc =$desc. "</p>";


            $desc= $desc."<p>";
            $desc =$desc. "<b>Date Found:</b> $datefound";
            $desc =$desc. "</p>";
    
            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Location:</b> $location $curlocation";
            $desc =$desc. "</p>"; 

            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Previous Location:</b> $prevlocation";
            $desc =$desc. "</p>";
 

            $desc= $desc."<p>";
            $desc =$desc. "<b>Received On:</b> $receivedon";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Description:</b> $description";
            $desc =$desc. "</p>"; 


            $desc= $desc."<p>";
            $desc =$desc. "<b>Size:</b> $size";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Color:</b> $color";
            $desc =$desc. "</p>";
            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Pattern:</b> $pattern";
            $desc =$desc. "</p>";
            
            
            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Weight:</b> $weight";
            $desc =$desc. "</p>";
            

            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Tail:</b> $tail";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Coat:</b> $coat";
            $desc =$desc. "</p>";

            $desc= $desc."<p>";
            $desc =$desc. "<b>Ears:</b> $ears";
            $desc =$desc. "</p>";
            
            $desc= $desc."<p>";
            $desc =$desc. "<b>Eyes:</b> $eyes";
            $desc =$desc. "</p>";


            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Declawed:</b> $declawed";
            $desc =$desc. "</p>";

            $desc= $desc."<p class='hidden' style='display:none;'>";
            $desc =$desc. "<b>Contact:</b> $contact $deptPhoneNumbers";
            $desc =$desc. "</p>";


            $desc= $desc."[TheChamp-Sharing]"; 

            
 

            // $desc=$desc. "[wpdevart_facebook_comment facebook_app_id="162201617625715"  title_text="Facebook Comment" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="random" locale="en_US" count_of_comments="2" ]";




          if(!empty( $species) && ( strcasecmp (   $species ,   "cat" )==0  || strcasecmp (   $species ,   "dog" )==0 )     ){ 
             postToWordPress($jsonPlace, $species,$title,$desc,$categoryId,$photo,$imagefilename);
          }

         } 
      
    

  }
  catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  } 

}


function performInsertionShelther($filename,$locationfilename){


  try{
      echo "BEGIN "  ; 

      //read from file 
      $folderPath= "./dist/";
      $json_data = file_get_contents($folderPath . $filename );  
      $jsonList = json_decode($json_data, true);

 
      $json_placedata = file_get_contents($folderPath . $locationfilename );  
      $jsonPlace = json_decode($json_placedata, true);


      $totalRow= count($jsonPlace);

      echo "<br>The  totalRow is: $totalRow  <br>"  ;  
   
 
          $curRow =   $jsonPlace;  
   
          $name =isJsonKeyExist($curRow,"name")? $curRow["name"] :"-";
          $address =isJsonKeyExist($curRow,"address")? $curRow["address"] :"-";
          $phone =isJsonKeyExist($curRow,"phone")? $curRow["phone"] :"-";
          $contact =isJsonKeyExist($curRow,"contact")? $curRow["contact"] :"-";
    
             echo "<br>The name is:  <br>"  . $name;   

            // $desc="[mashshare]";
            $desc=" ";


 
           $desc= $desc."<p style='listing-shelter-name'>";
            $desc =$desc. "<b>Name:</b> $name";
            $desc =$desc. "</p>";


            $desc= $desc."<p style='listing-shelter-address'>";
            $desc =$desc. "<b>Address:</b> $address";
            $desc =$desc. "</p>";

            $desc= $desc."<p style='listing-shelter-phone'>";
            $desc =$desc. "<b>Phone:</b> $phone";
            $desc =$desc. "</p>";


            // $desc= $desc."<p>";
            // $desc =$desc. "<b>Contact:</b> $contact";
            // $desc =$desc. "</p>";
 


            $desc= $desc."[TheChamp-Sharing]"; 

             

           postToWordPressShelter($jsonPlace,  $name,$desc  );
    
    

  }
  catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  } 

}

function downloadPic($title, $thumbnail){
  $dataPic = file_get_contents($thumbnail);
  $filePath = "./wp-content/uploads/". $title. ".jpg";
  file_put_contents($filePath, $dataPic );
  return $filePath;
 
}

function grab_image($url,$imagefilename){

  if(!empty($imagefilename)){
    $url  = "http://bringpawshome.com/".$imagefilename ;
  }

  $res=null; 
  $res2=null;

  try{ 
    $res2=file_get_contents($url);     
  }
  catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  } 

  try{
 
 


   header("Content-Type: image/jpeg");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $ip="104.131.13.177";
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));

    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
    $res = curl_exec($ch);
    $rescode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    curl_close($ch) ;
 }
  catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
  }  
  return empty($res2)? $res: $res2 ;
}


function generateFeaturedImage( $image_url, $post_id , $imagefilename  ){
    $upload_dir = wp_upload_dir();
    // $image_data = file_get_contents($image_url);
     $image_data =  grab_image($image_url ,$imagefilename);
  
     sleep(1.93);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);


    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array( 
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    
    $res2= set_post_thumbnail( $post_id, $attach_id );

  return $attach_id;
}

 
 function postToWordPressShelter($jsonPlace, $name,$desc  ){
  global $wpdb; 
     

  $post_if = $wpdb->get_var("SELECT count(post_title) FROM $wpdb->posts WHERE post_title  =  $name ");

    $post = get_page_by_title(   $name, OBJECT, 'job_listing' ); 
     
    if ( $post->ID !=  0 ) {
             echo '<br/>DUPLICATE: pet post found; SKIPPING ...' .$title  ; 
              //wp_delete_post( $post->ID  );    
    }
    else{
    //  return false;
       $new_post = array(
       'post_title' =>  $name ,  
       'post_content' =>   $desc,
       'post_status' => 'publish',
       'post_date' => date('Y-m-d H:i:s'),  
       'post_author' => $user_ID,
       'post_type' => 'job_listing',
       'post_category' =>  array($categoryId)
       );
       $post_id = wp_insert_post($new_post);

       echo 'Successfully inserted id:  $post_id , post:'.$title  ;
       print_r( $new_post) ; 


      $address =isJsonKeyExist($jsonPlace,"address")? $jsonPlace["address"] :"-";   
      $name =isJsonKeyExist($jsonPlace,"name")? $jsonPlace["name"] :"-";   
      $email =isJsonKeyExist($jsonPlace,"email")? $jsonPlace["email"] :"-";   

      $lat =isJsonKeyExist($jsonPlace,"lat")? $jsonPlace["lat"] :"-";
      $lat =isJsonKeyExist($jsonPlace,"lat")? $jsonPlace["lat"] :"-";  
      $phone =isJsonKeyExist($jsonPlace,"phone")? $jsonPlace["phone"] :"-";  
      $contact =isJsonKeyExist($jsonPlace,"contact")? $jsonPlace["contact"] :$email ;

    //shelther and species 
      $name=empty( $name)?"Unknown-Shelther":$name;
       $name=str_replace("  "," ",$name);
 


      $email =isJsonKeyExist($jsonPlace,"email")? $jsonPlace["email"] :"-";   

      $startHourWeekday =isJsonKeyExist($jsonPlace,"startHourWeekday")? $jsonPlace["startHourWeekday"] :"-";   
      $endHourWeekday =isJsonKeyExist($jsonPlace,"endHourWeekday")? $jsonPlace["endHourWeekday"] :"-";   

      $startHourWeekend =isJsonKeyExist($jsonPlace,"startHourWeekend")? $jsonPlace["startHourWeekend"] :"-";   
      $endHourWeekend =isJsonKeyExist($jsonPlace,"endHourWeekend")? $jsonPlace["endHourWeekend"] :"-";    


  
     
        echo "<br>" ;
        echo "category-->". $name; 


       wp_set_object_terms( $post_id, $name, "job_listing_category"  );   

      add_post_meta(   $post_id,   "_filled",  0 ); 
      add_post_meta(   $post_id,   "_featured",  "1" ); 
      add_post_meta(   $post_id,   "geolocation_lat",  $lat ); 
      add_post_meta(   $post_id,   "geolocation_long",    $lng ); 
      add_post_meta(   $post_id,   "geolocation_street",  $name .$address ); 
 
      add_post_meta(   $post_id,   "geolocation_formatted_address",  $name . $address ); 
      add_post_meta(   $post_id,   "geolocation_street_number",  "-wip-" );  
      add_post_meta(   $post_id,   "geolocation_street",  "-wip-" );  
      add_post_meta(   $post_id,   "geolocation_city",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_state_short",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_state_long",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_postcode",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_country_short",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_country_long",  "-wip-"  );  
      
      add_post_meta(   $post_id,   "geolocated",  "-wip-"  );  
      

      add_post_meta(   $post_id,   "_job_location",  $address ); 
 

      $startHourWeekday =isJsonKeyExist($jsonPlace,"startHourWeekday")? $jsonPlace["startHourWeekday"] :"-";   
      $endHourWeekday =isJsonKeyExist($jsonPlace,"endHourWeekday")? $jsonPlace["endHourWeekday"] :"-";    

      $startHourWeekend =isJsonKeyExist($jsonPlace,"startHourWeekend")? $jsonPlace["startHourWeekend"] :"-";   
      $endHourWeekend =isJsonKeyExist($jsonPlace,"endHourWeekend")? $jsonPlace["endHourWeekend"] :"-";    

      $jobhours=  array( array('start'=>$startHourWeekend, 'end'=>$endHourWeekend),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday), array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday), array('start'=>$startHourWeekend, 'end'=>$endHourWeekend),)    ;
 

      add_post_meta(   $post_id,   "_job_hours",  ($jobhours)  ); 

      add_post_meta(   $post_id,   "_job_author",  1 );

      add_post_meta(   $post_id,   "_application",  $contact );
      add_post_meta(   $post_id,   "_phone",   $phone ); 
      add_post_meta(   $post_id,   "_company_instagram",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_twitter",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_facebook",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_website",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_website",    $jsonPlace["link"]   ); 
      add_post_meta(   $post_id,   "_thumbnail_id",  "-wip-"  );



      add_post_meta(   $post_id,   "_claimed",  0 ); 
      add_post_meta(   $post_id,   "_breed",  $species    ); 
      add_post_meta(   $post_id,   "rating",  0 );
      add_post_meta(   $post_id,   "rating",  0 );
  

 

       //add meta data place
       //$jsonPlace
    }  

 }

 function postToWordPress($jsonPlace,  $species,$title,$desc,$categoryId,$filepath,$imagefilename){
  global $wpdb; 
     

  $post_if = $wpdb->get_var("SELECT count(post_title) FROM $wpdb->posts WHERE post_title  =  $title ");

    $post = get_page_by_title(   $title, OBJECT, 'job_listing' ); 
     
    if ( $post->ID !=  0 ) {
             echo '<br/>DUPLICATE: pet post found; SKIPPING ...' .$title  ; 
              //wp_delete_post( $post->ID  );    
             return false;
    }
    else{
    //  return false;
       $new_post = array(
       'post_title' =>  $title ,  
       'post_content' =>   $desc,
       'post_status' => 'publish',
       'post_date' => date('Y-m-d H:i:s'),  
       'post_author' => $user_ID,
       'post_type' => 'job_listing',
       'post_category' =>  array($categoryId)
       );
       $post_id = wp_insert_post($new_post);

       echo 'Successfully inserted id:  $post_id , post:'.$title  ;
       print_r( $new_post) ; 


      $address =isJsonKeyExist($jsonPlace,"address")? $jsonPlace["address"] :"-";   
      $name =isJsonKeyExist($jsonPlace,"name")? $jsonPlace["name"] :"-";   
      $email =isJsonKeyExist($jsonPlace,"email")? $jsonPlace["email"] :"-";   

      $lat =isJsonKeyExist($jsonPlace,"lat")? $jsonPlace["lat"] :"-";
      $lat =isJsonKeyExist($jsonPlace,"lat")? $jsonPlace["lat"] :"-";  
      $phone =isJsonKeyExist($jsonPlace,"phone")? $jsonPlace["phone"] :"-";  
      $contact =isJsonKeyExist($jsonPlace,"contact")? $jsonPlace["contact"] :$email ;

    //shelther and species
      $species=empty( $species)?"":$species;
      $name=empty( $name)?"Unknown-Shelther":$name;
       $name=str_replace("  "," ",$name);
       $species=str_replace("  "," ",$species);

       $species=str_replace(" dead","", ($species));
       $species=str_replace(" Dead","", ($species)); 
       $species=str_replace("dead","", ($species));
       $species=str_replace("Dead","", ($species)); 


      $email =isJsonKeyExist($jsonPlace,"email")? $jsonPlace["email"] :"-";   

      $startHourWeekday =isJsonKeyExist($jsonPlace,"startHourWeekday")? $jsonPlace["startHourWeekday"] :"-";   
      $endHourWeekday =isJsonKeyExist($jsonPlace,"endHourWeekday")? $jsonPlace["endHourWeekday"] :"-";   

      $startHourWeekend =isJsonKeyExist($jsonPlace,"startHourWeekend")? $jsonPlace["startHourWeekend"] :"-";   
      $endHourWeekend =isJsonKeyExist($jsonPlace,"endHourWeekend")? $jsonPlace["endHourWeekend"] :"-";    


        if(  strpos( strtolower($species) ,"dog" )=== false  && strpos( strtolower($species) , "cat" )=== false    ){   
            $species="";
        }
     
        echo "<br>" ;
        echo "category-->". $name;
        echo "<br>" ; 
        echo "species-->". $species;


       wp_set_object_terms( $post_id, $name, "job_listing_category"  );  
       wp_set_object_terms( $post_id, $species, "job_listing_region" ); 

      add_post_meta(   $post_id,   "_filled",  0 ); 
      add_post_meta(   $post_id,   "_featured",  0 ); 
      add_post_meta(   $post_id,   "geolocation_lat",  $lat ); 
      add_post_meta(   $post_id,   "geolocation_long",    $lng ); 
      add_post_meta(   $post_id,   "geolocation_street",  $name .$address ); 
 
      add_post_meta(   $post_id,   "geolocation_formatted_address",  $name . $address ); 
      add_post_meta(   $post_id,   "geolocation_street_number",  "-wip-" );  
      add_post_meta(   $post_id,   "geolocation_street",  "-wip-" );  
      add_post_meta(   $post_id,   "geolocation_city",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_state_short",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_state_long",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_postcode",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_country_short",  "-wip-"  );  
      add_post_meta(   $post_id,   "geolocation_country_long",  "-wip-"  );  
      
      add_post_meta(   $post_id,   "geolocated",  "-wip-"  );  
      

      add_post_meta(   $post_id,   "_job_location",  $address ); 
      $jobhours= "a:7:{i:0;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:1;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:2;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:3;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:4;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:5;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}i:6;a:2:{s:5:'start';s:8:'11:30 am';s:3:'end';s:8:'11:00 pm';}}"  ;


      $startHourWeekday =isJsonKeyExist($jsonPlace,"startHourWeekday")? $jsonPlace["startHourWeekday"] :"-";   
      $endHourWeekday =isJsonKeyExist($jsonPlace,"endHourWeekday")? $jsonPlace["endHourWeekday"] :"-";    

      $startHourWeekend =isJsonKeyExist($jsonPlace,"startHourWeekend")? $jsonPlace["startHourWeekend"] :"-";   
      $endHourWeekend =isJsonKeyExist($jsonPlace,"endHourWeekend")? $jsonPlace["endHourWeekend"] :"-";    

      $jobhours=  array( array('start'=>$startHourWeekend, 'end'=>$endHourWeekend),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday), array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday),array('start'=>$startHourWeekday, 'end'=>$endHourWeekday), array('start'=>$startHourWeekend, 'end'=>$endHourWeekend),)    ;

// "hours": [
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ],
//         [ "11:30 am", "11:00 pm" ]
//       ],

      add_post_meta(   $post_id,   "_job_hours",  ($jobhours)  ); 

      add_post_meta(   $post_id,   "_job_author",  1 );

      add_post_meta(   $post_id,   "_application",  $contact );
      add_post_meta(   $post_id,   "_phone",   $phone ); 
      add_post_meta(   $post_id,   "_company_instagram",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_twitter",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_facebook",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_website",   "-wip-"  ); 
      add_post_meta(   $post_id,   "_company_website",    $jsonPlace["link"]   ); 
      add_post_meta(   $post_id,   "_thumbnail_id",  "-wip-"  );



      add_post_meta(   $post_id,   "_claimed",  0 ); 
      add_post_meta(   $post_id,   "_breed",  $species    ); 
      add_post_meta(   $post_id,   "rating",  0 );
      add_post_meta(   $post_id,   "rating",  0 );
  

      $picId= generateFeaturedImage( $filepath,   $post_id, $imagefilename );
      $gallery="[gallery ids=$picId]" ;
      add_post_meta(   $post_id,   "_gallery",  $gallery ); 
      add_post_meta(   $post_id,   "_gallery_images",  $gallery ); 
      // add_post_meta(   $post_id,   "_gallery_images",  "a:3:{i:0;s:89:"http://www.bringpawshome.com/wp-content/uploads/2017/03/listing-hoxton-featured-image.jpg";i:1;s:76:"http://www.bringpawshome.com/wp-content/uploads/2017/03/listing-hoxton-1.jpg";i:2;s:76:"http://www.bringpawshome.com/wp-content/uploads/2017/03/listing-hoxton-2.jpg";}" ); 

       //add meta data place
       //$jsonPlace
    }  

  }



// performInsertionShelther("ListingHumaneSociety.txt","ListingHumaneSociety.txt"); 
// performInsertionShelther("ListingBurienCares.txt","ListingBurienCares.txt");
// performInsertionShelther("ListingPaws.txt","ListingPaws.txt");
// performInsertionShelther("ListingEverett.txt","ListingEverett.txt");
// performInsertionShelther("ListingAuburnValley.txt","ListingAuburnValley.txt");
// performInsertionShelther("ListingKingCounty.txt","ListingKingCounty.txt");
// performInsertionShelther("ListingKitsap.txt","ListingKitsap.txt");
// performInsertionShelther("ListingSeatle.txt","ListingSeatle.txt");


// performInsertion("ResultBurienCares.txt","ListingBurienCares.txt"); 
// performInsertion("ResultAuburnValley.txt","ListingAuburnValley.txt");
// performInsertion("ResultSeatle.txt","ListingSeatle.txt"); 

//no image . duplicate.
  //? performInsertion("ResultHumaneSocietyHumaneSocietyDogs.txt","ListingHumaneSociety.txt");
  //? performInsertion("ResultHumaneSocietyHumaneSocietyCats.txt","ListingHumaneSociety.txt"); 


performInsertion("ResultPaws.txt","ListingPaws.txt"); 
// ?performInsertion("ResultEverett.txt","ListingEverett.txt");
// ? performInsertion("ResultKingCounty.txt","ListingKingCounty.txt");
 // ? performInsertion("ResultKitsap.txt","ListingKitsap.txt");

?>