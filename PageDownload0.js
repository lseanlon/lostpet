var page = require('webpage').create();
var loadInProgress = false;
var testindex = 0;
var system = require('system');

if (system.args.length === 1) {
    //-console.log('Usage: google.js <some Query>');
    phantom.exit(1);
} else {
    q = system.args[1];
}


// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onAlert = function(msg) {
    console.log('alert!!> ' + msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("load started");
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    } else {
        console.log("load finished");
    }
};

var steps = [
    function() {
        page.open(q);
    },

    function() {
        console.log('Answers:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function() {

                try {

                    if (!$) {
                        $ = jQuery;
                    }
                    if (!$) {
                        console.log('html: ' + document.body.outerHTML);
                    }

                    console.log('html: ' + $('body').html());
                    // console.log('html: ' +  document.body.outerHTML);
                    
var frameCount = page.framesCount + 1
var html = page.frameContent + '\n\n'
for (var i = 1; i < frameCount; ++i) {
  page.switchToFrame(i)
  html += page.frameContent + '\n\n'
} console.log( "iframe html:  " + html); 
                    // var iframelen = $('iframe').length
                    // for (var x = 0; x < iframelen; x++) {
                    //     var iframedata = $('iframe').eq(x).contents().find('body').html()
                    //     console.log('iframe data: ' + iframedata);  
                    //      console.log( "iframe data:  " + $('iframe').eq(x).contents().html()); 
                    // }
                } catch (e) { 
                    console.log('html: ' + document.body.outerHTML);
                }


            });
        }, 0);
    },
    function() {
        console.log('Exiting');
    }
];

interval = setInterval(function() {
    if (!loadInProgress && typeof steps[testindex] == "function") {
        console.log("step " + (testindex + 1));
        steps[testindex]();

        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}, 7250);
