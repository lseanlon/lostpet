var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var strlist = "";
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListBurienCares.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;
    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow', curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp(commonFolder, function(err) {
        if (err) return cb(err);
        console.log("create folder path", commonFolder);
    });

    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 30000) {
        finaltimer = 10000;
    }
    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        console.log('start download ', urlLink);


        var contacturl = "http://www.buriencares.org/contact/"
        var commandRun = 'sleep 0.1s && curl "' + (contacturl) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download contacturl', contacturl);

                if (!output) {
                    return;
                }

                var $ = cheerio.load(output);


                var sanitizeDash = function(_val) {
                    return ($("<p>" + _val + "</p>").text());
                }
                var companyInfo = { "link": urlLink, "ref": ref ? ref : "" };



                if ($('.sqs-block-content').eq(2).find('p').eq(0)) {
                    companyInfo.rawaddress = ($('.sqs-block-content').eq(2).find('p').eq(0).html());
                    companyInfo.address = sanitizeDash($('.sqs-block-content').eq(2).find('p').eq(0).html());
                }
                if ($('.sqs-block-content').eq(2).find('p').eq(1)) {
                    companyInfo.rawcontact = (($('.sqs-block-content').eq(2).find('p').eq(1)).html());
                    companyInfo.contact = sanitizeDash(($('.sqs-block-content').eq(2).find('p').eq(1)).html());
                }

                if (companyInfo.rawaddress) {
                    companyInfo.name = (companyInfo.rawaddress).split("&nbsp;")[0];
                    // companyInfo.name = (companyInfo.name).split("/ ")[0]; 
                }

                companyInfo.name = "Burien C.A.R.E.S";

                if (companyInfo.rawcontact) {
                    companyInfo.phone = (companyInfo.rawcontact).split("<br>")[0];
                    companyInfo.phone = sanitizeDash(companyInfo.phone);
                }

                companyInfo.lat = 47.4674728
                companyInfo.lng = -122.3482682


                companyInfo.startHourWeekday= "09:00 am" 
                companyInfo.endHourWeekday= "06:00 pm" 
                companyInfo.startHourWeekend= "09:00 am" 
                companyInfo.endHourWeekend= "01:00 pm"

    
                var filename = "./dist/ListingBurienCares.txt";
                fs.writeFileSync(filename, JSON.stringify(companyInfo));
                console.log("written to file ", filename);



            });

        } catch (e) {
            console.log(e);

        }




        var commandRun = 'sleep 1.1s && phantomjs  --web-security=no PageDownload0.js "' + (urlLink) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(curRow);

                var filename = "./dist/ResultBurienCares.txt";
                var liststr = "[{}" + strlist + "]";
                fs.writeFileSync(filename, liststr);
                console.log("written to file ", filename);
                downloadedEntryPathList.push(curRow);


            });

        } catch (e) {
            console.log(e);

        }





    }, finaltimer);





});
var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        return ($("<p>" + _val + "</p>").text());
    }

    curLink = urllink;
    var curRow = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };

    var detailFolder = commonFolder + "/" + _elem.ref;
    mkdirp(detailFolder, function(err) {
        if (err) return cb(err);
        console.log("create folder path", detailFolder);
    });



    $('.image-list .image').each(function(idx, elemelem) {

        if ($(this).find('.image-desc a')) {

            var detailurl = $(this).find('.image-desc a').prop('href');
            curRow.id = detailurl.replace("/", '');
            curRow.name = detailurl.replace("/", '');
            curRow.detailurl = "http://www.buriencares.org" + detailurl;


            curRow.species = _elem.ref.replace('BurienCares','');  
            curRow.species =  curRow.species.replace('Dogs','Dog');  
            curRow.species =  curRow.species.replace('Cats','Cat');  
            curRow.detailurlfile = detailFolder + "/" + curRow.id + ".txt";

            if (fs.existsSync(curRow.detailurlfile)) {

                console.log('skipped download urlLink', (curRow.detailurl));
                console.log('skipped download urlPath', curRow.detailurlfile);



            } else {


                var commandRun = '  curl "' + (curRow.detailurl) + '" > "' + curRow.detailurlfile + '"  ';
                try {
                    console.log('commandRun', commandRun);
                    exec(commandRun, function(status, output) {
                        console.log('Exit status:', status);

                        console.log('done download urlLink', (curRow.detailurl));
                        console.log('done download urlPath', curRow.detailurlfile);

                        if (!detailurl || !output) {
                            return;
                        }

                    });

                } catch (e) {
                    console.log(e);

                }
            }
        }
        if ($(this).find('img')) {
            curRow.photo = ($(this).find('img').prop('src')).replace("https", "http");;
            curRow.photo = curRow.photo.replace("?format=500w", "");
        } 
        console.log(" curRow ", curRow);
        listData.push(curRow);
        strlist = strlist + "," + JSON.stringify(curRow);


    });



};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
