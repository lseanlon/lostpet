var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListKitsap.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var finalList = [];
var downloadedEntryPathList = [];
//download all the first page listing
 
var handleSubPages = function(_elem) {
  if(!_elem || !_elem.detailurlfile){
    return;
  }
    var fileContent = fs.readFileSync(_elem.detailurlfile, 'utf8');
    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        return ($("<p>" + _val + "</p>").text());
    }
 
 
    if ($('table').eq(0).find('p.textemphasis').eq(0).length>=1  ) {
        console.log( $('table').eq(0).find('p.textemphasis').eq(0).html());
        _elem.species= $('table').eq(0).find('p.textemphasis').eq(0).html().toLowerCase().replace('found' ,'')
    }

    if ($('table').eq(1).find('tr').eq(2).find('td').eq(3).find()>=1 ) {
        _elem.photo = sanitizeDash($('table').eq(1).find('tr').eq(2).find('td').eq(3).find().html());
    }
    if ($('table').eq(1).find('tr').eq(2).find('td.text')>=1 ) {
        _elem.breed = sanitizeDash($('table').eq(1).find('tr').eq(2).find('td.text').html());
    }
    if ($('table').eq(1).find('tr').eq(4).find('td.text')>=1 ) {
        _elem.sterialize = sanitizeDash($('table').eq(1).find('tr').eq(4).find('td.text').html());
    }
    if ($('table').eq(1).find('tr').eq(5).find('td.text')>=1 ) {
        _elem.age = sanitizeDash($('table').eq(1).find('tr').eq(5).find('td.text').html());
    }
    if ($('table').eq(1).find('tr').eq(6).find('td.text')>=1 ) {
        _elem.color = sanitizeDash($('table').eq(1).find('tr').eq(6).find('td.text').html());
    } 
    if ($('table').eq(1).find('tr').eq(7).find('td.text')) {
        _elem.marking = sanitizeDash($('table').eq(1).find('tr').eq(7).find('td.text').html());
    } 
    if ($('table').eq(1).find('tr').eq(8).find('td.text')>=1 ) {
        _elem.description = sanitizeDash($('table').eq(1).find('tr').eq(8).find('td.text').html());
    } 
    if ($('table').eq(1).find('tr').eq(11).find('td.text')>=1 ) {
        _elem.notes = sanitizeDash($('table').eq(1).find('tr').eq(11).find('td.text').html());
    } 
    if ($('table').eq(1).find('tr').eq(12).find('td.text')>=1 ) {
        _elem.email = sanitizeDash($('table').eq(1).find('tr').eq(12).find('td.text').html());
    }   

     

    console.log('row',_elem);
    finalList.push(_elem);

};
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;


    var listdata = fs.readFileSync('./dist/ResultKitsap.txt', 'utf8');
    listdata = JSON.parse(listdata);
    for (var i = 0; i < listdata.length; i++) {
        var curelem = listdata[i]
        if (curelem && curelem.link) { 
            handleSubPages(curelem);
        }
    }


    var filename = "./dist/ResultKitsap.txt";
    fs.writeFileSync(filename, JSON.stringify(finalList));
    console.log("write to file ", filename);





});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
