
<!DOCTYPE html>
<html>
<script src="include/Common.js" 
  language="javascript" 
  type="text/javascript"></script>

<SCRIPT LANGUAGE=JAVASCRIPT>
<!-- hide script from old browsers

// ******************************************************************************************
//               ALL CONTENTS COPYRIGHT � HLP, INC. 1998-2015                               *
//                                                                                          *
// It is illegal to reproduce or distribute copyrighted material without the                *
// permission of the copyright owner.                                                       *
//                                                                                          *
// Accessing images or text provided on Web sites does not give you any rights              *
// to use them as you wish. Only the copyright owner, or the owner's legal agent,           *
// can give you permission to copy, distribute, or publicly display protected               *
// material.                                                                                *
//                                                                                          *
// The No Electronic Theft Act, known as the NET Act, was enacted in 1997 to facilitate     *
// prosecutions of Internet copyright piracy.                                               *
// The NET Act makes it illegal to reproduce or distribute copyrighted works,               *
// such as software programs and musical recordings,                                        *
// even if the defendant acts without a commercial purpose or for private financial gain.   *
//                                                                                          *
// Connection to the PetHarbor engine by any means is legal via license with HLP ONLY.      *   
// ******************************************************************************************

//-->
</script>
<NOSCRIPT>
This document contains JavaScript.  You may have disabled JavaScript, 
or perhaps your browser does not support it.  Please be aware that 
some of the useful features of our site depend upon this technology.
You can download (for free) a newer browser that supports JavaScript
from either <A href="www.microsoft.com">Microsoft</a> (www.microsoft.com) 
or <a href="www.netscape.com">Netscape</a> (www.netscape.com).
</noscript><SCRIPT LANGUAGE="JavaScript"><!--var sPassAlong = new String("") ;sPassAlong = "searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG" ;//--></SCRIPT>
<head>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <TITLE>www.PetHarbor.com Animal Search: STRAY OR FOUND </TITLE><link rel="stylesheet" type="text/css" href="http://www.petharbor.com/clientimages/tacm/tacoma1.css">
  <meta http-equiv="imagetoolbar" content="no">
  <!--<meta addname='X-XSS-Protection:0' content='IE=edge' http-equiv='X-UA-Compatible'/>-->
      <TITLE>PetHarbor.com: Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found.</TITLE>
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="txt/html; charset=ISO-8859-1">
    <meta name="Description" content="PetHarbor.com: National Adoptable and Lost & Found database. Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found. Data from hundreds of animal shelters in the US and Canada. Breed Search, Lost and Found pet matching service.">
    <meta name="Abstract" content="PetHarbor.com: Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found.">
    <meta name="Keywords" content="pictures of cats,animal shelter,pet finder,cute puppies,humane society,spca,adopt a pet,animal shelters,dog rescue,animal rescue,adopt a dog,adopt a puppy,adopt a cat,lost animals,pet harbor,petharbor,pet adoption,lost dogs,petfinder,lost cats">
    <meta name="classification" content="pictures of cats,animal shelter,pet finder,cute puppies,humane society,spca,adopt a pet,animal shelters,dog rescue,animal rescue,adopt a dog,adopt a puppy,adopt a cat,lost animals,pet harbor,petharbor,pet adoption,lost dogs,petfinder,lost cats">
    <meta name="Copyright" content="Copyright � 1998-2015 HLP, Inc.">
    <meta name="Owner" content="HLP, Inc.">
    <meta name="Robots" content="all">
    <meta name="Distribution" content="Global">
    <meta name="Revisit-After" content="15 days">
    <meta name="Rating" content="General">
    <meta name="Reply-to" content="support@PetHarbor.com">



  <script src="include/common.js" language="javascript" type="text/javascript"></script>
  <script src="include/SaveSearch.js" language="javascript" type="text/javascript"></script>
  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="include/jquery.mosaicflow.min.js"></script>-->
</head>
    <body>
  <body><form name="frmResults" action="" method="get" ID="frmResults"><table width="100%" class="TableTitle TableTitleMain"><tr><td align="center" class="TableTitle"><font class="Title">Current Search: STRAY OR FOUND DOGS</div></td></tr></table><center>We found 41 matches. Click on a column-header to sort by that column.<BR><font class="Instructions">Click on the picture for detailed information:</font></center><TABLE class="ResultsTable" align=center BORDER=0><TR ALIGN=CENTER><TD class="TableTitle">Picture</td><TD class="TableTitle"><A Alt="Click on Column Header to Sort" Title="Click on Column Header to Sort" HREF="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&NewOrderBy=Description&PAGE=1">Description</a></td><TD class="TableTitle"><A Alt="Click on Column Header to Sort" Title="Click on Column Header to Sort" HREF="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&NewOrderBy=Time%20At%20Shelter&PAGE=1">Time At Shelter</a></td></tr><TR ALIGN=CENTER><TD class="TableContent1"><A Alt="Click on this picture to see more information about this animal" Title="Click on this picture to see more information about this animal" HREF="detail.asp?ID=A515334&LOCATION=TACM&searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG"><IMG HEIGHT = "200" SRC="get_image.asp?RES=thumb&ID=A515334&LOCATION=TACM" oncontextmenu="return false"></a></td><TD class="TableContent1"><font class="big" color=#82bd42"> ID #A515334</font><br>Pit Bull Terrier mix<br>Male  &#124;  4 months<br>Black  &#124;  043</td><TD class="TableContent1">00 Years<br>00 Months<br>01 Days</td></tr></table><BR><CENTER>Page 5 of 5</center><CENTER><A href="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&PAGE=4">Previous Page</A>&nbsp&nbsp&nbsp<A href="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&PAGE=1">1</A> | <A href="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&PAGE=2">2</A> | <A href="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&PAGE=3">3</A> | <A href="results.asp?searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_DOG&PAGE=4">4</A> | 5</center><!-- 
  <table class="Command" style="margin: 0 auto; width: 550px;">
    <tr>
      <td colspan="5">
        <p class="step" style="text-align: center; font-weight: bold;">
          Customize This View</p>
      </td>
    </tr>
    <tr class="commandRowA">
      <td>
        <b>Image Size</b>
      </td>
      <td>
        <input type="radio" name="ImageSize" value="l" onclick="NoteChoice('imght', '300')" />
        Large
      </td>
      <td>
        <input type="radio" name="ImageSize" value="m" onclick="NoteChoice('imght', '200')" />
        Medium
      </td>
      <td>
        <input type="radio" name="ImageSize" value="s" onclick="NoteChoice('imght', '120')" />
        Small
      </td>
      <td>
      </td>
    </tr>
    <tr class="commandRowB">
      <td>
        <b>Image Resolution</b>
      </td>
      <td>
        <input type="radio" name="ImageRes" value="l" onclick="NoteChoice('imgres', 'thumb')" />
        Low
      </td>
      <td width="18%">
        <input type="radio" name="ImageRes" value="h" onclick="NoteChoice('imgres', 'detail')" />
        High
      </td>
      <td>
      </td>
      <td>
      </td>
    </tr>
    <tr class="commandRowA">
      <td>
        <b>Rows Per Page </b>
      </td>
      <td>
        <input type="radio" name="Rows" value="1" onclick="NoteChoice('rows', '1')" />
        1
      </td>
      <td>
        <input type="radio" name="Rows" value="5" onclick="NoteChoice('rows', '5')" />
        5
      </td>
      <td>
        <input type="radio" name="Rows" value="10" onclick="NoteChoice('rows', '10')" />
        10
      </td>
      <td>
        <input type="radio" name="Rows" value="25" onclick="NoteChoice('rows', '25')" />
        25
      </td>
    </tr>
    <tr>
      <td colspan="5" style="text-align: center;">
        <input type="button" name="cmdReForm" value="Re-Form Page" onclick="ReForm()" />
      </td>
    </tr>
  </table>
  -->
  <div class="copyright" style="text-align: center; margin-top: 20px;">
    Powered By <a target="_blank" href="http://www.PetHarbor.com">
      <img src="images/lh_11.png">
      <b>www.PetHarbor.com</b> </a>
  </div>
  <div class="copyright" style="text-align: center;">Contents copyright &copy;HLP Inc 1997-2017<br/>HLP, Inc. and PetHarbor make no assertions about the availability, behavior or condition of any animal.</div>
  </form>
  <!--<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>-->
  <!--<script type="text/javascript">
_uacct = "UA-2258493-1";
urchinTracker();
</script>-->
  <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-2258493-1']);
      _gaq.push(['_trackPageview']);

      (function () {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

      $(window).load(function () {
          var $paragraphs = $('.gridResult'), heights = [];
          $paragraphs.each(function () {
              heights.push($(this).height());
          });

          var maxHeight = Math.max.apply(this, heights);

          $paragraphs.height(maxHeight);

          $('.gridResult').css('vertical-align', 'top');
      });

      //$(document).ready(function () {
      //    //  $('a[class=TableContent1]').attr('target', '_blank');

      //    $('.TableContent1 a').attr('target', '_blank');
      //    $('.TableContent2 a').attr('target', '_blank');
      //});


  </script>
</body>
<script src="resultsFooter.js" language="javascript" type="text/javascript"></script>
</html>
