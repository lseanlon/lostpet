

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>

</title>
    <title>Animal Details</title>
    <link id="stylesheet" rel="stylesheet" href="https://ws.petango.com/WebServices/adoptablesearch/css/styles.css" type="text/css" />
</head>
<body>
    <!-- Google Tag Manager -->

    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-55MWRN"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':

        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],

    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =

    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);

})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>

    <!-- End Google Tag Manager -->

    
            
<div class="detail-body">

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="detail-animal-name"><div class="detail-animal-name"><span id="main_lbName">Charm</span></div></td>
        <td class="detail-link" style="text-align:right; vertical-align:top;"><a href="" onclick="history.go(-1); return false;"><span id="main_LabelReturnToListing"><font size="3">Return to Listing</font></span></a></td>
    </tr>
</table>


<table border="0">
	<tr>
		<td valign="top"><img id="main_imgAnimalPhoto" class="detail-animal-photo" src="//media.petango.com/sms/photos/1117/2118802c-3d37-4b37-8feb-f7635e60e0b8.jpg" /></td>
		<td valign="top">
			<table class="detail-table" border="0" cellspacing="0">
				<tr id="main_trAnimalID">
	<td class="detail-label" align="right" width="130"><b>Animal ID</b></td>
	<td class="detail-value"><span id="main_lblID">34841545</span>&nbsp;</td>
</tr>

				<tr id="main_trSpecies">
	<td class="detail-label" align="right" width="130"><b>Species</b></td>
	<td class="detail-value"><span id="main_lblSpecies">Cat</span>&nbsp;</td>
</tr>

				<tr id="main_trBreed">
	<td class="detail-label" align="right" width="130"><b>Breed</b></td>
	<td class="detail-value"><span id="main_lbBreed">Snowshoe/Mix</span>&nbsp;</td>
</tr>

				<tr id="main_trAge">
	<td class="detail-label" align="right" width="130"><b>Age</b></td>
	<td class="detail-value"><span id="main_lbAge">7 years</span>&nbsp;</td>
</tr>

				<tr id="main_trSex">
	<td class="detail-label" align="right" width="130"><b>Gender</b></td>
	<td class="detail-value"><span id="main_lbSex">Female</span>&nbsp;</td>
</tr>

				<tr id="main_trDateFound">
	<td class="detail-label" align="right" width="130"><b>Date Found</b></td>
	<td class="detail-value"><span id="main_lblFoundDate">3/11/2017</span>&nbsp;</td>
</tr>

				<tr id="main_trLocationFound">
	<td class="detail-label" align="right" width="130"><b>Location Found</b></td>
	<td class="detail-value"><span id="main_lblFoundLocation">16th St near church, Everett</span>&nbsp;</td>
</tr>

				
				<tr id="main_trSize">
	<td class="detail-label" align="right" width="130"><b>Size</b></td>
	<td class="detail-value"><span id="main_lblSize">Medium</span></td>
</tr>

				<tr id="main_trColor">
	<td class="detail-label" align="right" width="130"><b>Color</b></td>
	<td class="detail-value"><span id="main_lblColor">Cream / Lavender</span></td>
</tr>

				<tr id="main_trColorPattern">
	<td class="detail-label" align="right" width="130"><b>Color Pattern</b></td>
	<td class="detail-value"><span id="main_lblColorPattern">Point</span></td>
</tr>

				
				
				
				
				
				
				
				<tr id="main_trDeclawed">
	<td class="detail-label" align="right" width="130"><b>Declawed</b></td>
	<td class="detail-value"><span id="main_lblDeclawed">No</span></td>
</tr>

				
				
				
				<tr id="main_trReportType">
	<td class="detail-label" align="right" width="130"><b>Report Type</b></td>
	<td class="detail-value"><span id="main_lblReportType">Animal in Custody</span></td>
</tr>

                
		</table>
       </td>
	</tr>
</table>

        <table id="main_tblDsc" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
                    <img name="Animal_r1_c1" src="images/Animal_r1_c1.gif" alt="" border="0" height="22"
                        width="20" />
                </td>
		<td>
                    <img name="Animal_r1_c2" src="images/Animal_r1_c2.gif" alt="" border="0" height="22"
                        width="100%" />
                </td>
		<td>
                    <img name="Animal_r1_c3" src="images/Animal_r1_c3.gif" alt="" border="0" height="22"
                        width="24" />
                </td>
	</tr>
	<tr bgcolor="#eceff6">
		<td background="images/Animal_r2_c1.gif">
                </td>
		<td>
                    
                    
                </td>
		<td background="images/Animal_r2_c3.gif">
                </td>
	</tr>
	<tr>
		<td>
                    <img name="Animal_r3_c1" src="images/Animal_r3_c1.gif" alt="" border="0" height="23"
                        width="20" />
                </td>
		<td>
                    <img name="Animal_r3_c2" src="images/Animal_r3_c2.gif" alt="" border="0" height="23"
                        width="100%" />
                </td>
		<td>
                    <img name="Animal_r3_c3" src="images/Animal_r3_c3.gif" alt="" border="0" height="23"
                        width="24" />
                </td>
	</tr>
</table>

<!-- Begin comScore Tag -->
<script>
    document.write(unescape("%3Cscript src='" + (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js' %3E%3C/script%3E"));
</script>

<script>
  COMSCORE.beacon({
    c1:2,
    c2:6745171,
    c3:"",
    c4:"",
    c5:"",
    c6:"",
    c15:""
  });
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1" />
</noscript>
<!-- End comScore Tag -->

</div>

    
</body>
</html>
