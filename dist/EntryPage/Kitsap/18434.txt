<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Information about Found Dog</title>
<style type="text/css">
<!--
body {
	background-color: #F8F7F5;
}
-->
</style>
<link href="bodystyleklp.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.html"><img src="images/klpheader2.jpg" width="950" height="201" /></a>
</p>
<table width="100%" border="2" cellspacing="1" cellpadding="2">
  <tr>
    <td width="25%"><h2>Resources</h2>      
      <p><h3>Search Tips</h3></p>
      <p><a href="klpsearchtipsdog.html">Dog Search</a></p>
      <p><a href="klpsearchtipscat.html">Cat Search</a></p>      
      <p><a href="klpwtdfounddog.html">Found Dog Hints</a></p>
      <p><a href="klpwtdfoundcat.html">Found Cat Hints</a></p></td>
    <td width="75%" rowspan="2" align="left"><h1>Detail Listing</h1>
          <a href="klpsearchresults.php?petType=dog&gender=unknown&location=any&submit=submit&lorf=found">Back to search listing</a>
              <p class="textemphasis">Found Dog </p>
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
        	<td class="tabletitle" colspan="3" align="center"><p>Share this listing on Facebook
            <a href="#" onclick="
                  window.open(
                    'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
                    'facebook-share-dialog', 
                    'width=626,height=436'); 
                  return false;"><img src="images/facebook-share-button.jpg" width="57" height="18"/></a></p>
            </td>
        </tr>
        <tr>
          <td width="20%" class="tabletitle">Name</td>
          <td width="51%" class="text"></td>
          <td width="29%" rowspan="11" align="center">
             <img src="upload/58876ce53c8ed.jpg" height="200"/>
            		  </td>
        </tr>
        <tr>
          <td class="tabletitle">Breed</td>
          <td class="text">Blue Heeler Mix</td>
        </tr>
        <tr>
          <td class="tabletitle">Gender</td>
          <td class="text">male</td>
        </tr>
        <tr>
          <td class="tabletitle">Sterilization Status:</td>
          <td class="text">unknown</td>
        </tr>
        <tr>
          <td class="tabletitle">Age</td>
          <td class="text">Approx 3 Months</td>
        </tr>
        <tr>
          <td class="tabletitle">Coloring</td>
          <td class="text">
		  White Tri-color</td>
        </tr>
        <tr>
          <td class="tabletitle">Special Markings</td>
          <td class="text">KHS Animal ID: 61732</td>
        </tr>
        <tr>
          <td class="tabletitle">Identification Type</td>
          <td class="text">
		  no collar, no tags, no microchip</td>
        </tr>
        <tr>
          <td class="tabletitle">Date found</td>
          <td class="text">2017-01-23</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="tabletitle">Location found</td>
                    <td class="text">Tidewind Loop, Town: Kingston (98346)</td>
        </tr>
        <tr>
          <td class="tabletitle">Notes</td>
          <td class="text"></td>
        </tr>
        <tr>
          <td class="tabletitle">Contact eMail</td>
          <td class="text">customerservice@kitsap-humane.org</td>
          <td rowspan="3" align="center"><p class="textemphasis">KitsapLostPets.org</p>
          <p class="textemphasis">info@kitsaplostpets.org</p>
          <p class="tabletitle">Record ID: <span class="text">18434</span></p></td>
        </tr>
      </table>
            <p>&nbsp;</p>
      <p class="textemphasis">&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>      
      <p>&nbsp;</p>      
    <p>&nbsp;</p>      <p>&nbsp;</p>      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
