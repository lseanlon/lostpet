

<html>
<script src="include/Common.js" 
  language="javascript" 
  type="text/javascript"></script>

<SCRIPT LANGUAGE=JAVASCRIPT>
<!-- hide script from old browsers

// ******************************************************************************************
//               ALL CONTENTS COPYRIGHT � HLP, INC. 1998-2015                               *
//                                                                                          *
// It is illegal to reproduce or distribute copyrighted material without the                *
// permission of the copyright owner.                                                       *
//                                                                                          *
// Accessing images or text provided on Web sites does not give you any rights              *
// to use them as you wish. Only the copyright owner, or the owner's legal agent,           *
// can give you permission to copy, distribute, or publicly display protected               *
// material.                                                                                *
//                                                                                          *
// The No Electronic Theft Act, known as the NET Act, was enacted in 1997 to facilitate     *
// prosecutions of Internet copyright piracy.                                               *
// The NET Act makes it illegal to reproduce or distribute copyrighted works,               *
// such as software programs and musical recordings,                                        *
// even if the defendant acts without a commercial purpose or for private financial gain.   *
//                                                                                          *
// Connection to the PetHarbor engine by any means is legal via license with HLP ONLY.      *   
// ******************************************************************************************

//-->
</script>
<NOSCRIPT>
This document contains JavaScript.  You may have disabled JavaScript, 
or perhaps your browser does not support it.  Please be aware that 
some of the useful features of our site depend upon this technology.
You can download (for free) a newer browser that supports JavaScript
from either <A href="www.microsoft.com">Microsoft</a> (www.microsoft.com) 
or <a href="www.netscape.com">Netscape</a> (www.netscape.com).
</noscript><SCRIPT LANGUAGE="JavaScript"><!--var sPassAlong = new String("") ;sPassAlong = "searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_CAT" ;//--></SCRIPT>
	<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="http://www.petharbor.com/clientimages/tacm/tacoma1.css">
    <meta http-equiv="imagetoolbar" content="no"> 
    <TITLE>PetHarbor.com: Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found.</TITLE>
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="txt/html; charset=ISO-8859-1">
    <meta name="Description" content="PetHarbor.com: National Adoptable and Lost & Found database. Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found. Data from hundreds of animal shelters in the US and Canada. Breed Search, Lost and Found pet matching service.">
    <meta name="Abstract" content="PetHarbor.com: Animal Shelter adopt a pet; dogs, cats, puppies, kittens! Humane Society, SPCA. Lost & Found.">
    <meta name="Keywords" content="pictures of cats,animal shelter,pet finder,cute puppies,humane society,spca,adopt a pet,animal shelters,dog rescue,animal rescue,adopt a dog,adopt a puppy,adopt a cat,lost animals,pet harbor,petharbor,pet adoption,lost dogs,petfinder,lost cats">
    <meta name="classification" content="pictures of cats,animal shelter,pet finder,cute puppies,humane society,spca,adopt a pet,animal shelters,dog rescue,animal rescue,adopt a dog,adopt a puppy,adopt a cat,lost animals,pet harbor,petharbor,pet adoption,lost dogs,petfinder,lost cats">
    <meta name="Copyright" content="Copyright � 1998-2015 HLP, Inc.">
    <meta name="Owner" content="HLP, Inc.">
    <meta name="Robots" content="all">
    <meta name="Distribution" content="Global">
    <meta name="Revisit-After" content="15 days">
    <meta name="Rating" content="General">
    <meta name="Reply-to" content="support@PetHarbor.com">


 
</head>
<body><script language="javascript">function Back(){window.history.back() ; }</script>For a shorter link to this pet click <a target="_new" href="pet.asp?uaid=TACM.A515570">here</a>.<br><div class="a2a_kit a2a_default_style"> 
<a class="a2a_dd" href="http://www.addtoany.com/share_save?linkurl=http%3A%2F%2Fwww.PetHarbor.com%2Fpet.asp%3Fuaid%3DTACM.A515570&amp;linkname=PetHarbor PetTACM.A515570">Share</a> 
<span class="a2a_divider"></span> 
<iframe src="http://www.facebook.com/plugins/like.php?href=pet.asp?uaid=TACM.A515570&amp;layout=standard&amp;show_faces=true&amp;width=450&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe></div> 
<script type="text/javascript"> 
var a2a_config = a2a_config || {}; 
a2a_config.linkname = "PetHarbor Pet"; 
a2a_config.linkurl = "http://www.PetHarbor.com/pet.asp?uaid=TACM.A515570"; 
</script> 
<script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script><TABLE class="DetailTable" width="100%" align="top" HEIGHT="220"><TR><TD align="center" width=333><IMG HEIGHT = "300" SRC="get_image.asp?RES=Detail&ID=A515570&LOCATION=TACM" oncontextmenu="return false"></td><TD class="DetailDesc"><font class="Title">This CAT&nbsp;-&nbsp;ID#A515570<BR><BR></font>Breed:  Domestic Shorthair<BR><BR>Gender: Female<BR><BR>Size: Small<BR><br>Age: 2 years<br><br>Color: Gray Tabby<br><br>Location: C077</td></tr></table><table width="100%" class="DetailTable"><tr><td><div align="center"><b>Shelter Staff made the following comments about this animal:</b></div></td></tr><tr><td><div align="center">I would love to meet you. I am a Female Domestic Shorthair. The shelter staff think I am about 2 years and 0 months old. I have been at the shelter since Mar 16, 2017.<br><br></div></td></tr></table><table width="100%" class="DetailTable"><tr><td width=333><div align="center"><a href="javascript:Back()">Back</a></div></td><td div align="center">For more information about this animal, call:<BR><a href=site.asp?ID=TACM&searchtype=LOST&start=4&nopod=1&stylesheet=http://www.petharbor.com/clientimages/tacm/tacoma1.css&friends=1&samaritans=1&nosuccess=1&rows=10&imght=200&imgres=thumb&tWidth=200&view=sysadm.v_tacm&nobreedreq=1&nomax=1&nocustom=1&fontface=arial&fontsize=10&zip=98409&miles=200&shelterlist='TACM'&atype=&where=type_CAT>Humane Society for Tacoma and Pierce County</a>&nbsp;at&nbsp;(253) 383-2733<br>Ask for information about animal ID number A515570</div></TD></TR></table>
    
    <form id="frmEmail" name="frmEmail">

    
    
    <div align="center" style="margin-top: 15px;">
    <table class="Command" border="0" id="Table1" style="font-size: 12px;">
      <tr>
        <td colspan="2">    
          <center><h2>Email This Animal's Info to a Friend</h2></center>
        </td>
      </tr>
      <tr>
        <td align="left">
          Your Email: 
        </td>
        <td>
          <input id="txtSourceEmail" type="text" size="60" name="txtSourceEmail"/>
        </td>
      </tr>
      <tr>
        <td align="left">
          Friend's Email:
        </td>
        <td>
          <input id="txtTargetEmail" type="text" size="60" name="txtTargetEmail"/>
        </td>
      </tr>
      <tr>
        <td align="left">
          A Message (optional):
        </td>
        <td>
          <input id="txtMsg" type="text" size="60" name="txtMsg"/>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td align="center">
          It is a violation of this site's policy to enter any email address other than your own as the sender.  
          <br/><b>Use of this service implies your agreement with this condition.</b>
          <br/>If you are sending to an animal rescue group, please check with them before sending. They may already have it.
          <br/>We reserve the right to ban any user we deem to be using these services irresponsibly.
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="button" name="cmdEmail" value="Email" onclick="JavaScript:mailThis()" id="cmdEmail"/>
        </td>
      </tr>
    </table> 
    </div>
    


    <br>
        
   <font class="Tiny">
   <div align="center" class="copyright">Powered By <a target=_blank href="http://www.PetHarbor.com">
          <img src="images/lh_11.png"
            border="0"> <b>www.PetHarbor.com</b> </a></div> 
   </font>
  <div class="copyright" style="text-align: center;">Contents copyright &copy;HLP Inc 1997-2017<br/>HLP, Inc. and PetHarbor make no assertions about the availability, behavior or condition of any animal.</div>
    </form>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2258493-1";
urchinTracker();
</script>
</body>

<SCRIPT LANGUAGE="JavaScript">var sAID;sAID = "A515570";var sLOC;sLOC = "TACM";</script>

<script src="detailFooter.js" 
  language="javascript" 
  type="text/javascript"></script>

</font>
</html>
