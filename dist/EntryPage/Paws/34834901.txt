

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>

</title>
    <title>Animal Details</title>
    <link id="stylesheet" rel="stylesheet" href="//www.petango.com/WebServices/adoptablesearch/css/styles.css" type="text/css" />
</head>
<body>
    <!-- Google Tag Manager -->

    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-55MWRN"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':

        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],

    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =

    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);

})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>

    <!-- End Google Tag Manager -->

    
            
<div class="detail-body">

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="detail-animal-name"><div class="detail-animal-name"><span id="main_lbName">Goose</span></div></td>
        <td class="detail-link" style="text-align:right; vertical-align:top;"><a href="" onclick="history.go(-1); return false;"><span id="main_LabelReturnToListing"><font size="3">Return to Listing</font></span></a></td>
    </tr>
</table>


<table border="0">
	<tr>
		<td valign="top"><img id="main_imgAnimalPhoto" class="detail-animal-photo" src="//media.petango.com/sms/photos/1963/f268bfec-3e8f-4edb-abb5-93ba1b5b5d84.jpg" /></td>
		<td valign="top">
			<table class="detail-table" border="0" cellspacing="0">
				<tr id="main_trAnimalID">
	<td class="detail-label" align="right" width="130"><b>Animal ID</b></td>
	<td class="detail-value"><span id="main_lblID">34834901</span>&nbsp;</td>
</tr>

				<tr id="main_trSpecies">
	<td class="detail-label" align="right" width="130"><b>Species</b></td>
	<td class="detail-value"><span id="main_lblSpecies">Cat</span>&nbsp;</td>
</tr>

				<tr id="main_trBreed">
	<td class="detail-label" align="right" width="130"><b>Breed</b></td>
	<td class="detail-value"><span id="main_lbBreed">Domestic Medium Hair/Mix</span>&nbsp;</td>
</tr>

				<tr id="main_trAge">
	<td class="detail-label" align="right" width="130"><b>Age</b></td>
	<td class="detail-value"><span id="main_lbAge">1 year</span>&nbsp;</td>
</tr>

				<tr id="main_trSex">
	<td class="detail-label" align="right" width="130"><b>Gender</b></td>
	<td class="detail-value"><span id="main_lbSex">Male</span>&nbsp;</td>
</tr>

				<tr id="main_trDateFound">
	<td class="detail-label" align="right" width="130"><b>Date Found</b></td>
	<td class="detail-value"><span id="main_lblFoundDate">3/10/2017</span>&nbsp;</td>
</tr>

				
				
				<tr id="main_trSize">
	<td class="detail-label" align="right" width="130"><b>Size</b></td>
	<td class="detail-value"><span id="main_lblSize">Medium</span></td>
</tr>

				<tr id="main_trColor">
	<td class="detail-label" align="right" width="130"><b>Color</b></td>
	<td class="detail-value"><span id="main_lblColor">Grey</span></td>
</tr>

				
				
				<tr id="main_trLocation">
	<td class="detail-label" align="right" width="130"><b>Location</b></td>
	<td class="detail-value"><span id="main_lblLocation">Cat Room</span></td>
</tr>

				<tr id="main_trSite">
	<td class="detail-label" align="right" width="130"><b>Site</b></td>
	<td class="detail-value"><span id="main_lblSite">Lynnwood</span></td>
</tr>

				<tr id="main_trWeight">
	<td class="detail-label" align="right" width="130"><b>Weight</b></td>
	<td class="detail-value"><span id="main_lblWeight">10 pound</span></td>
</tr>

				
				
				
				<tr id="main_trDeclawed">
	<td class="detail-label" align="right" width="130"><b>Declawed</b></td>
	<td class="detail-value"><span id="main_lblDeclawed">No</span></td>
</tr>

				
				
				
				<tr id="main_trReportType">
	<td class="detail-label" align="right" width="130"><b>Report Type</b></td>
	<td class="detail-value"><span id="main_lblReportType">Animal in Custody</span></td>
</tr>

                
		</table>
       </td>
	</tr>
</table>

        <table id="main_tblDsc" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
                    <img name="Animal_r1_c1" src="images/Animal_r1_c1.gif" alt="" border="0" height="22"
                        width="20" />
                </td>
		<td>
                    <img name="Animal_r1_c2" src="images/Animal_r1_c2.gif" alt="" border="0" height="22"
                        width="100%" />
                </td>
		<td>
                    <img name="Animal_r1_c3" src="images/Animal_r1_c3.gif" alt="" border="0" height="22"
                        width="24" />
                </td>
	</tr>
	<tr bgcolor="#eceff6">
		<td background="images/Animal_r2_c1.gif">
                </td>
		<td>
                    <table id="main_tbl24PetWatch" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px;" bgcolor="#eceff6">
			<tr bgcolor="#eceff6">
				<td bgcolor="#eceff6">
                                <a id="main_lnk24PetWatch" href="http://www.24petwatch.com" target="_blank"><img src="../adoptablesearch/images/24PW-Pet-Protection-Services-logo-150x84-with-white-space.png" class="logo" border="0" /></a>
                            </td>
				<td class="detail-animal-desc" style="padding-left: 10px; vertical-align: bottom;" bgcolor="#eceff6">
                                Make sure they can always find their way home with 24PetWatch lost pet recovery services. For more information visit <a href='http://www.24petwatch.com' target='_blank'>www.24PetWatch.com</a> or call 1.866.597.2424.
                            </td>
			</tr>
		</table>
		
                    
                </td>
		<td background="images/Animal_r2_c3.gif">
                </td>
	</tr>
	<tr>
		<td>
                    <img name="Animal_r3_c1" src="images/Animal_r3_c1.gif" alt="" border="0" height="23"
                        width="20" />
                </td>
		<td>
                    <img name="Animal_r3_c2" src="images/Animal_r3_c2.gif" alt="" border="0" height="23"
                        width="100%" />
                </td>
		<td>
                    <img name="Animal_r3_c3" src="images/Animal_r3_c3.gif" alt="" border="0" height="23"
                        width="24" />
                </td>
	</tr>
</table>

<!-- Begin comScore Tag -->
<script>
    document.write(unescape("%3Cscript src='" + (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js' %3E%3C/script%3E"));
</script>

<script>
  COMSCORE.beacon({
    c1:2,
    c2:6745171,
    c3:"",
    c4:"",
    c5:"",
    c6:"",
    c15:""
  });
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1" />
</noscript>
<!-- End comScore Tag -->

</div>

    
</body>
</html>
