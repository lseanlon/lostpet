var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListEverett.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var finalList = [];
var downloadedEntryPathList = [];
//download all the first page listing

var handleSubPages = function(_elem) {



    var fileContent = fs.readFileSync(_elem.detailurlfile, 'utf8');
    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        return ($("<p>" + _val + "</p>").text());
    }
    if ($('.detail-animal-photo')) {
        _elem.photo = ($('.detail-animal-photo').prop('src'));
        if (_elem.photo.indexOf("http") < 0) {
            _elem.photo = "http:" + _elem.photo;
        }
    }

    if ($('#trDateFound td.detail-value')) {
        _elem.datefound = sanitizeDash($('#trDateFound td.detail-value').html());
    }

    if ($('#trDateFound td.detail-value')) {
        _elem.datefound = sanitizeDash($('#trDateFound td.detail-value').html());
    }

    if ($('#trLocation td.detail-value')) {
        _elem.location = sanitizeDash($('#trLocation td.detail-value').html());
    }

    if ($('#trSize td.detail-value')) {
        _elem.size = sanitizeDash($('#trSize td.detail-value').html());
    }

    if ($('#trColor td.detail-value')) {
        _elem.color = sanitizeDash($('#trColor td.detail-value').html());
    }
    if ($('#trColorPattern td.detail-value')) {
        _elem.pattern = sanitizeDash($('#trColorPattern td.detail-value').html());
    }
    if ($('#trWeight td.detail-value')) {
        _elem.weight = sanitizeDash($('#trWeight td.detail-value').html());
    }

    if ($('#trTail td.detail-value')) {
        _elem.tail = sanitizeDash($('#trTail td.detail-value').html());
    }
    if ($('#trCoat td.detail-value')) {
        _elem.coat = sanitizeDash($('#trCoat td.detail-value').html());
    }
    if ($('#trEyes td.detail-value')) {
        _elem.eyes = sanitizeDash($('#trEyes td.detail-value').html());
    }
    if ($('#trEars td.detail-value')) {
        _elem.ears = sanitizeDash($('#trEars td.detail-value').html());
    }
    if ($('#trDeclawed td.detail-value')) {
        _elem.declawed = sanitizeDash($('#trDeclawed td.detail-value').html());
    }

    if ($('#trReportType td.detail-value')) {
        _elem.reporttype = sanitizeDash($('#trReportType td.detail-value').html());
    }



    console.log('row', _elem);
    finalList.push(_elem);

};
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;


    var listdata = fs.readFileSync('./dist/ResultEverett.txt', 'utf8');
    listdata = JSON.parse(listdata);
    for (var i = 0; i < listdata.length; i++) {
        var curelem = listdata[i]
        if (curelem && curelem.link) {
            handleSubPages(curelem);
        }
    }


    var filename = "./dist/ResultEverett.txt";
    fs.writeFileSync(filename, JSON.stringify(finalList));
    console.log("write to file ", filename);





});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
