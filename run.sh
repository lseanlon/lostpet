###
echo "================================================"
echo "BEGIN DAILY Run "
echo "================================================"
	echo "Moving data extracted result..." 
	cp -rf ./dist/ ..
	sleep 1.12s

	echo "Moving automation program result..."
	cp -rf postRowAutomatically.php .. 
	sleep 1.12s


	echo "Clean old cached data"
	# sh clean.sh
	# 
	echo "Begin generating data"
	 sh runAuburnValley.sh
	 sleep 1.12s
 
	 sh runKingCounty.sh
	 sleep 1.12s
 
	 sh runSeatle.sh 
	 sleep 1.12s

	 sh runKitsap.sh
	 sleep 1.12s
 
	 sh runPaws.sh 
	 sleep 1.12s 


	 sh runHumaneSociety.sh 
	 sleep 1.12s


	 sh runEverett.sh 
	 sleep 1.12s
 
	 sh runBurienCares.sh 
	 sleep 1.12s


	cp -rf ./dist/ ..
	sleep 1.12s


	echo "Uploading into wordpress data..." 
	curl http://bringpawshome.com/postRowDeleteAutomatically.php
	curl http://bringpawshome.com/postRowAutomatically.php
	sleep 1.12s
 
echo "================================================"
echo "END DAILY Run "
echo "================================================"