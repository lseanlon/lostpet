var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    urllink = urlLink;
    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow', curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp("./dist/EntryPage", function(err) {
        if (err) return cb(err);
        console.log("create folder path", "./dist/EntryPage");
    });

    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 30000) {
        finaltimer = 10000;
    }
    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        console.log('start download ', urlLink);




        var commandRun = 'phantomjs  --web-security=no PageDownload0.js "' + (urlLink) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(curRow);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);



    }, finaltimer);

});
var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        //take out all html   
        return ($("<p>" + _val + "</p>").text());
    }

    curLink = urllink;
    var curRow = { "link": curLink, "ref": _elem.ref ? _elem.ref : "" };

    if ($('#deptPhoneNumbers')) {
        // curRow.deptPhoneNumbers = ($('#deptPhoneNumbers').html());
    }

    var strlist = "";

    $('.list-item').each(function(idx, elemelem) {

        var curLink = $(this).find('.list-animal-name a').prop('href');
        curLink = urllink;

        if ($(this).find('.list-animal-name a')) {

            curRow.name = sanitizeDash($(this).find('.list-animal-name a').html());
            if (curRow.name == 'null') {
                curRow.name = null;
            }
        }


        if ($(this).find('.list-animal-photo')) {
            curRow.photo = ($(this).find('.list-animal-photo').prop('src'));
        }
        if ($(this).find('.list-animal-id')) {
            curRow.id = sanitizeDash($(this).find('.list-animal-id').html());
        }
        if ($(this).find('.list-anima-species')) {
            curRow.species = sanitizeDash($(this).find('.list-anima-species').html());
        }
        if ($(this).find('.list-animal-sexSN')) {
            curRow.sex = sanitizeDash($(this).find('.list-animal-sexSN').html());
        }
        if ($(this).find('.list-animal-breed')) {
            curRow.breed = sanitizeDash($(this).find('.list-animal-breed').html());
        }
        if ($(this).find('.list-animal-age')) {
            curRow.age = sanitizeDash($(this).find('.list-animal-age').html());
        }

        if ($(this).find('.list-animal-foundtype')) {
            curRow.foundtype = sanitizeDash($(this).find('.list-animal-foundtype').html());
        }

        // console.log(" curRow ", curRow);
        if (curRow.id && curRow.name) {
            listData.push(curRow);
            strlist  =strlist + ","+ JSON.stringify(curRow);
        }

    });

    var filename = "./dist/ResultSeatle.txt";
    var liststr = "[{}" + strlist + "]" ;
    fs.writeFileSync(filename, liststr);


};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
 