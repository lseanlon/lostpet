var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListHumaneSociety.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var finalList = [];
var downloadedEntryPathList = [];
//download all the first page listing

var handleSubPages = function(_elem) {

    if (!_elem || !_elem.detailurlfile) {
        return;
    }
    var fileContent = fs.readFileSync(_elem.detailurlfile, 'utf8');
    var $ = cheerio.load(fileContent);


    var sanitizeDash = function(_val) {
        return ($("<p>" + _val + "</p>").text());
    }


    if ($('.DetailTable img')) {
        _elem.photo = "http://petharbor.com/" + $('.DetailTable img').prop('src');


        var filename = "dist/humanesocietyimage" + _elem.name.replace(/[^a-z0-9]/gi, '_').toLowerCase() + ".jpg"
        _elem.filename = filename;


        var url = (_elem.photo);

        download(url, "./dist").then((data) => {
            fs.writeFileSync(filename, data);
            console.log('done download data', data);
            console.log('done download - ' + url + ' into ' + filename);
        });



    }

    if ($('.DetailTable .DetailDesc')) {

        var detailhtml = $('.DetailTable .DetailDesc').html();
        if (detailhtml) {

            var listhtml = detailhtml.split("<br><br>")
            _elem.location = listhtml[listhtml.length - 1]
            _elem.color = listhtml[listhtml.length - 2]
            _elem.age = listhtml[listhtml.length - 3]
            _elem.size = listhtml[listhtml.length - 4]

        }
    }

    console.log('row', _elem);
    finalList.push(_elem);

};
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];
    ref = ref.replace("\r", "");
    urllink = urlLink;


    var listdata = fs.readFileSync('./dist/ResultHumaneSociety' + ref + '.txt', 'utf8');
    listdata = JSON.parse(listdata);
    for (var i = 0; i < listdata.length; i++) {
        var curelem = listdata[i]
        if (curelem && curelem.link) {
            handleSubPages(curelem);
        }
    }


    var filename = "./dist/ResultHumaneSociety" + ref + ".txt";
    fs.writeFileSync(filename, JSON.stringify(finalList));
    console.log("write to file ", filename);





});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
