var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
var listData = [];
require('shelljs/global');
var commonFolder = "./dist/EntryPage";
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlListHumaneSociety.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urllink = "";
var ref = "";
var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    ref = urlInfo[2];
    urllink = urlLink;
    var companyInfo = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };

    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow', curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp(commonFolder, function(err) {
  
        console.log("create folder path", commonFolder);
    });

    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 30000) {
        finaltimer = 10120;
    }
   console.log('companyInfo', companyInfo)

    companyInfo.name = "Humane Society for Tacoma & Pierce County"
    companyInfo.address = " 2608 Center Street, Tacoma, WA 98409 ,Washington"
    companyInfo.contact = "(253) 383-2733"
    companyInfo.phone = "(253) 383-2733"
    companyInfo.fax = "253.383.2733"

    companyInfo.lat = 47.233843;
    companyInfo.lng = -122.4735427;

    companyInfo.startHourWeekday = "11:00 am"
    companyInfo.endHourWeekday = "05:30 pm"
    companyInfo.startHourWeekend = "09:00 am"
    companyInfo.endHourWeekend = "04:30 pm"


   console.log('companyInfo', companyInfo)
            
    var filename = "./dist/ListingHumaneSociety.txt";
    fs.writeFileSync(filename, JSON.stringify(companyInfo));
    console.log("written to file ", filename);
 

    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        console.log('start download ', urlLink);



        //listing and details page
        var commandRun = 'phantomjs  --web-security=no PageDownload0.js "' + (urlLink) + '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(output);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);


        

    }, finaltimer);





});
var handleSubPages = function(_html) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var $ = cheerio.load(_html);

    var sanitizeDash = function(_val) {
        //take out all html   
        return ($("<p>" + _val + "</p>").text());
    }

    curLink = urllink;

    //download all index page
    // ($('tbody:eq(1) tr:eq(0)').find('td:eq(0)')



    var maxRecPerPage = 10;
    if ($('.ResultsTable tr[align=CENTER]')) {
        maxRecPerPage = $('.ResultsTable tr[align=CENTER]').length - 1
    }



    var totalPage = 1
    if ($(':contains("Page 1 of ")')) {
        var $elem = $(':contains("Page 1 of ")');

        console.log('totalPage  $elem ', $elem);
        var lastIndex = $(':contains("Page 1 of ")').length - 1
        totalPage = $elem.eq(lastIndex).html()


        totalPage = totalPage.replace("Page 1 of", "");
        totalPage = parseInt(totalPage);
    }

    var totalrec = totalPage * maxRecPerPage;
    totalrec = isNaN(totalrec) ? 1 : totalrec



    if ($(':contains("Next Page")')) {
        var $elem = $(':contains("Next Page")');
        var lastIndex = $(':contains("Next Page")').length - 1
        urllink = $elem.eq(lastIndex).prop('href')
        urllink = removeURLParameter(urllink, "PAGE")
    }

    var pagelocalfolder = commonFolder + "/" + ref;

    mkdirp(pagelocalfolder, function(err) {
        if (err) return cb(err);
        console.log("create folder path", pagelocalfolder);
    });

    console.log('maxRecPerPage', maxRecPerPage);
    console.log('totalrec', totalrec);
    console.log('totalPage', totalPage);

    var generalData = { "pagelocalfolder": pagelocalfolder, "totalPage": totalPage, "totalrec": totalrec, "maxRecPerPage": maxRecPerPage }
    fs.writeFileSync("./dist/ListingIndexHumaneSociety"+ref+ ".txt", JSON.stringify(generalData));


    for (var y = 1; y <= totalPage; y++) {

        var pagelocalpath = pagelocalfolder + "/" + y + ".txt"
        var pageurl = "http://petharbor.com/" + urllink + "&PAGE=" + y;
        if (fs.existsSync(pagelocalpath)) {
            console.log('skip download pageurl', pageurl);
            console.log('skip download pagelocalpath', pagelocalpath);
        } else {

            //listing and details page
            var commandRun = 'curl "' + (pageurl) + '" > ' + pagelocalpath + '';

            console.log('commandRun', commandRun)
            console.log('--------------')
            try {

                exec(commandRun, function(status, output) {
                    console.log('Exit status:', status);
                    console.log('Program output:');

                    console.log('done download pageurl', pageurl);
                    console.log('done download pagelocalpath', pagelocalpath);

                    if (!pagelocalpath || !output) {
                        return;
                    }

                    // handleSubPages(output);


                });

            } catch (e) {
                console.log(e);

            }

        }

    }

    return ;



};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
